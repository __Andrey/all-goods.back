# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import *

admin.site.register(Site)
admin.site.register(Page)

admin.site.register(Goods)

admin.site.register(Session)
admin.site.register(Result)
