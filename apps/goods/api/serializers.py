# -*- coding: utf-8 -*-
from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField
from ..models import *


class SiteSerializer(ModelSerializer):
	class Meta:
		model = Site
		fields = '__all__'


class PageSerializer(ModelSerializer):
	site_id = PrimaryKeyRelatedField(source='site', queryset=Site.objects.all())
	class Meta:
		model = Page
		exclude = ('site',)


class GoodsSerializer(ModelSerializer):
	site_id = PrimaryKeyRelatedField(source='site', queryset=Site.objects.all())
	class Meta:
		model = Goods
		exclude = ('site',)


class SessionSerializer(ModelSerializer):
	class Meta:
		model = Session
		fields = '__all__'


class ResultSerializer(ModelSerializer):
	session_id = PrimaryKeyRelatedField(source='session' , queryset=Site.objects.all())
	page_id    = PrimaryKeyRelatedField(source='page'    , queryset=Site.objects.all())
	goods_id   = PrimaryKeyRelatedField(source='goods'   , queryset=Site.objects.all())
	class Meta:
		model = Result
		exclude = ('session', 'page', 'goods', )
