# -*- coding: utf-8 -*-
from rest_framework.viewsets import ModelViewSet
from .serializers import * 


class SiteViewSet(ModelViewSet):
	queryset         = Site.objects.all()
	serializer_class = SiteSerializer


class PageViewSet(ModelViewSet):
	queryset         = Page.objects.all()
	serializer_class = PageSerializer


class GoodsViewSet(ModelViewSet):
	queryset         = Goods.objects.all()
	serializer_class = GoodsSerializer


class SessionViewSet(ModelViewSet):
	queryset         = Session.objects.all()
	serializer_class = SessionSerializer


class ResultViewSet(ModelViewSet):
	queryset         = Result.objects.all()
	serializer_class = ResultSerializer
