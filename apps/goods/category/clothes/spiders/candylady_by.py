# -*- coding: utf-8 -*-
# third part packages
from scrapy import Request
from scrapy.contrib.spiders import Rule
from scrapy.contrib.linkextractors import LinkExtractor
# own packages
from base import BaseSpider


class Spider(BaseSpider):

    name = "candylady.by"
    allowed_domains = ["candylady.by"]
    rules = (
        Rule(LinkExtractor(allow=[r'\/catalog\/\d+']), follow=True),
        Rule(LinkExtractor(allow=[r'\/catalog\/model\/\d+']), 'parse_clothes'),
    )

    def start_requests(self):
        # В начале считываем курсы валют
        # и только после этого начинается парсинг остального
        return [Request("http://candylady.by/currency.php", callback=self.parse_currency_rates)]

    def parse_currency_rates(self, response):
        self.rates = {}
        for price in response.xpath("//select[@name='currency']/option"):
            rate     = price.xpath('@index').extract()[0]
            currency = price.xpath('@sign').extract()[0]
            self.rates[currency] = rate
        return Request("http://candylady.by/catalog/")

    def parse_clothes(self, response):
        item = self.getItem(response)

        item['maker']      = response.xpath("//div[@class='desc']/p[1]/text()").extract()[0]
        if item['maker'][-1] == '.':
            item['maker'] = item['maker'][:-1]

        item['model']      = response.xpath("//div[@class='card']/h1/text()").extract()
        item['model']      = item['model'][0].split()[1]

        item['image_urls'] = response.xpath("//a[@class='zoom_img']/@href").extract()
        item['image_urls'] = ['http://'+self.name+'/'+image for image in item['image_urls']]

        item['price'] = {}
        USD = float(response.xpath("//*[@itemprop='offers']/@data-val").extract()[0])
        item['price']['USD'] = USD
        for currency, rate in self.rates.items():
            item['price'][currency] = item['price']['USD'] * float(rate)

        #TODO: наличие

        return item

