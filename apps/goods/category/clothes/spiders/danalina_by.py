# -*- coding: utf-8 -*-
# third part packages
from scrapy.exceptions import DropItem
from scrapy.contrib.spiders import Rule
from scrapy.contrib.linkextractors import LinkExtractor
# own packages
from base import BaseSpider


class Spider(BaseSpider):

    name = "danalina.by"
    allowed_domains = ["danalina.by"]
    start_urls = ["http://danalina.by/catalog/",]
    rules = (
        Rule(LinkExtractor(allow=[r'\/catalog\/\?page=\d+']), follow=True),
        Rule(LinkExtractor(allow=[r'\/catalog\/\w+\/\d+\.html']), 'parse_clothes'),
    )

    def __init__(self):
        self.download_delay = 0.3
        super(Spider, self).__init__()

    def parse_clothes(self, response):
        item = self.getItem(response)

        item['maker'] = response.xpath("//p[@class='gr_text'][2]/a/text()").extract()[0]
        item['model'] = response.xpath("//p[@class='gr_text'][3]/text()").extract()[0]

        item['image_urls'] = response.xpath("//a[@class='lightbox_link big_image_link']/@href").extract()
        item['image_urls'] = ['http://'+self.name+image for image in item['image_urls']]


        item['price'] = {}
        # TODO: пока только одна валюта, с остальными проблема
        value    = response.xpath("//span[@class='price']/text()").extract()[0]
        currency = response.xpath("//span[@class='big_red']/text()").extract()[0].strip().upper()
        item['price'][currency] = float(value)

        #TODO: наличие

        return item