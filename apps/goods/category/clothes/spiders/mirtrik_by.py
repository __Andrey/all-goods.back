# -*- coding: utf-8 -*-
import re
import json
# third part packages
from scrapy.contrib.spiders import Rule
from scrapy.contrib.linkextractors import LinkExtractor
# own packages
from base import BaseSpider


class Spider(BaseSpider):

    name = "mirtrik.by"
    allowed_domains = ["mirtrik.by"]
    start_urls = ["http://www.mirtrik.by/shop",]
    rules = (
        Rule(LinkExtractor(allow=[r'\/shop\?page=\d+']), follow=True),
        Rule(LinkExtractor(allow=[r'\/product\/\d+']), 'parse_clothes'),
    )

    def __init__(self):
        self.download_delay = 0.3
        super(Spider, self).__init__()

    def parse_clothes(self, response):
        item = self.getItem(response)

        item['maker']      = response.xpath("//div[@id='product-info-block']/div[1]/a[1]/text()").extract()[0]

        item['model']      = response.xpath("//div[@id='product-model-number']/text()").extract()
        item['model']      = item['model'][0].split()[1]

        # не тянем все фото, они с водяными знаками и из-за этого от них мало пользы
        item['image_urls'] = response.xpath("//a[@class='highslide']/@href").extract()

        #
        item['price'] = {'USD' : float(response.xpath("//*[@itemprop='price']/text()").extract()[0]) }
        rates = json.loads(response.xpath("//input[@id='currencyDataField']/@value").extract()[0])
        for currency, rate in rates.items():
            if   currency == 'ru': currency = 'RUB'
            elif currency == 'by': currency = 'BYR'
            else:
                continue
            item['price'][currency] = item['price']['USD'] * float(rate['course'])

        #TODO: наличие

        return item