# -*- coding: utf-8 -*-
# third part packages
from scrapy.exceptions import DropItem
from scrapy.contrib.spiders import Rule
from scrapy.contrib.linkextractors import LinkExtractor
# own packages
from base import BaseSpider


class Spider(BaseSpider):

    name = "trikotazh.by"
    allowed_domains = ["trikotazh.by"]
    start_urls = ["http://trikotazh.by/catalog",]
    rules = (
        Rule(LinkExtractor(allow=[r'\/catalog\/page\/\d+']), follow=True),
        Rule(LinkExtractor(allow=[r'\/shop\/kind\/view\/id\/\d+']), 'parse_clothes'),
    )

    def parse_clothes(self, response):
        item = self.getItem(response)

        item['maker'] = response.xpath("//div[@class='product_info']/a/img/@alt").extract()
        if item['maker']:
            item['maker'] = item['maker'][0]
        else:
            return None

        item['model'] = response.xpath("//div[@class='product_text_list']/p[@class='product_text']/span/text()").extract()
        item['model'] = item['model'][0].split()[0]

        item['image_urls'] = response.xpath("//div[@class='clip']/a/@href").extract()
        item['image_urls'] = ['http://'+self.name+'/'+image for image in item['image_urls']]

        #TODO: где usd ?
        item['price'] = {}
        for price in response.xpath("//*[@itemprop='offers']"):
            value    = price.xpath('meta[@itemprop="price"]/@content').extract()[0]
            currency = price.xpath('meta[@itemprop="priceCurrency"]/@content').extract()[0]
            item['price'][currency] = float(value)

        #TODO: наличие

        return item