# -*- coding: utf-8 -*-
import re
# third part packages
from scrapy.contrib.spiders import Rule
from scrapy.contrib.linkextractors import LinkExtractor
# own packages
from base import BaseSpider


class Spider(BaseSpider):

    name = "weshalka.by"
    allowed_domains = ["weshalka.by"]
    start_urls = ["http://weshalka.by/index.php?id=14",]
    rules = (
        Rule(LinkExtractor(allow=[r'index\.php\?id=14&start=']), follow=True),
        Rule(LinkExtractor(allow=[r'index\.php\?id='], deny='start=', restrict_xpaths="//div[@class='item_box']/center/table/tbody/tr/td/center/a"), 'parse_clothes'),
    )

    def parse_clothes(self, response):
        item = self.getItem(response)

        item['model']      = response.xpath("//div[@class='product-info']/p[1]/text()").extract()[0]
        item['maker']      = response.xpath("//div[@class='product-info']/p[3]/text()").extract()[0][1:-1]
        item['image_urls'] = response.xpath("//img[@class='photo-gallery-item']/@src2").extract()
        item['image_urls'] = ['http://weshalka.by/'+image for image in item['image_urls']]

        item['price'] = {}
        USD = float(response.xpath("//span[@class='curr-price']/text()").extract()[0])
        rates = response.xpath("//script").re("(?P<rate>rate\['[A-Z]+'\] = [0-9.]+;)")
        pattern = re.compile("rate\['(?P<currency>[A-Z]+)'\] = (?P<value>[0-9.]+);")
        for rate in rates:
            currency, rate = pattern.findall(rate)[0]
            item['price'][currency] = USD * float(rate)

        #TODO: наличие

        return item