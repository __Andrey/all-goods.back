from django.apps import AppConfig


class Config(AppConfig):
	name  = 'apps.goods.category.smartphone'
	label = 'goods_smartphone'


default_app_config = 'apps.goods.category.smartphone.Config'
