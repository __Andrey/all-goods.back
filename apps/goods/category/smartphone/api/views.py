# -*- coding: utf-8 -*-
from rest_framework.viewsets import ModelViewSet
from .serializers import * 


class SmartphoneViewSet(ModelViewSet):
	queryset         = Smartphone.objects.all()
	serializer_class = SmartphoneSerializer

