# -*- coding: utf-8 -*-
from django.db.models   import Model, CASCADE, ForeignKey, CharField, TextField, DateTimeField
from ...models import Goods


class Smartphone(Model):
    title = CharField(max_length=256)

    def __str__(self):
        return self.title 
