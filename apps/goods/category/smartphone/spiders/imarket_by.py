import pytz
from scrapy.spiders        import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from django.conf           import settings
from django.utils          import timezone
from datetime              import datetime


class Spider(CrawlSpider):
    name            = "imarket.by - smartphone"
    allowed_domains = ["imarket.by"]
    start_urls      = ['https://imarket.by/catalog/mobilnye-telefony/']
    rules = (
        Rule(LinkExtractor(allow=[r'\/product\/([a-zA-Z0-9_.-]+)\/$']), 'parse_smartphone', follow=True),
    )

    def parse_smartphone(self, response):
        timestamp = datetime.utcnow()
        timestamp = timestamp.replace(tzinfo=pytz.utc)

        yield {
            'url'       : response.url, 
            'timestamp' : timestamp.isoformat(),
            'idx'       : response.css('h1[itemprop="name"]::attr(data-id)').extract_first(),
            'title'     : response.css('h1[itemprop="name"]::text').extract_first(),
            'desc'		: {},
            'price'     : {},
        }
