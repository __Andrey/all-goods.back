import pytz
from scrapy.spiders        import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from django.conf           import settings
from django.utils          import timezone
from datetime              import datetime


class Spider(CrawlSpider):
    name            = "mobistore.by - smartphone"
    allowed_domains = ["mobistore.by"]
    start_urls      = ['https://mobistore.by/mobilnye-telefony?page=all']
    rules = (
        Rule(LinkExtractor(allow=[r'([a-zA-Z0-9_.-]+)$'], restrict_css='a[itemprop="name"]'), 'parse_smartphone', follow=True),
    )

    def parse_smartphone(self, response):
        timestamp = datetime.utcnow()
        timestamp = timestamp.replace(tzinfo=pytz.utc)

        idx  = response.css('h1[itemprop="name"]::attr(data-product)').extract_first()
        name = response.css('h1[itemprop="name"]::text').extract_first()

        variants = response.css('.variant_line')
        for variant in variants:
            variant_id   = variant.css('::attr(data-variant-id)').extract_first()
            variant_name = variant.css('::attr(title)').extract_first()
            # if variant_name is None:
            #     variant_name = variant.css('label.variant_name::text').extract_first()
            if variant_name is None:
                variant_name = name
            else:
                variant_name = name + ' (' + variant_name + ')'

            yield {
                'url'       : response.url, 
                'timestamp' : timestamp.isoformat(),
                'idx'       : idx+':'+variant_id,
                'title'     : variant_name,
                'desc'		: {},
                'price'     : {},
            }
