import pytz
from scrapy.spiders        import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from django.conf           import settings
from django.utils          import timezone
from datetime              import datetime


class Spider(CrawlSpider):
    name            = "1k.by - smartphone"
    allowed_domains = ["phone.1k.by"]
    start_urls      = ['https://phone.1k.by/mobile/']
    rules = (
        Rule(LinkExtractor(allow=[r'\/mobile\/([a-zA-Z0-9_.-]+)\/([a-zA-Z0-9_.-]+).html$']), 'parse_smartphone', follow=True),
    )

    def parse_smartphone(self, response):
        timestamp = datetime.utcnow()
        timestamp = timestamp.replace(tzinfo=pytz.utc)

        data = {}
        full_desc = response.css('.fullInfo tr')
        for row in full_desc:
            parameter_id = row.css('.b-tip::attr(data-parameterid)').extract_first()

            if parameter_id == '686': # Операционная система 
                data['os'] = row.css('td::text').extract_first()

            elif parameter_id == '634': # Объем встроенной памяти 
                value = row.css('td::text').extract_first()
                data['memory'] = { 'value'     : value.split(' ')[0], 
                                   'value_type': value.split(' ')[1] }

        yield {
            'url'       : response.url, 
            'timestamp' : timestamp.isoformat(),
            'idx'       : response.css('#product-data::attr(data-productid)').extract_first(),
            'title'     : response.css('.b-page-ttl > h1::text').extract_first(),
            'desc'		: data,
            'price'     : {},
        }
