import pytz
from collections import defaultdict
from scrapy.spiders        import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from django.conf           import settings
from django.utils          import timezone
from datetime              import datetime


class Spider(CrawlSpider):
    name            = "svyaznoy.by - smartphone"
    allowed_domains = ["www.svyaznoy.by"]
    start_urls      = ['https://www.svyaznoy.by/catalog/phones/gsm/']
    rules = (
        Rule(LinkExtractor(allow=[r'\/catalog\/phones\/gsm\/([a-z])+\/([a-zA-Z0-9_.-])+$']), 'parse_smartphone', follow=False),
    )

    def parse_smartphone(self, response):
        timestamp = datetime.utcnow()
        timestamp = timestamp.replace(tzinfo=pytz.utc)

        desc = defaultdict(dict) 
        values = response.css('.b-product-view-box__left-column div.config-block > div::text').extract()
        for index, value in enumerate(values):
            if False:
                pass
            # -- cpu ------------------------------------------------------------------------------
            elif value.startswith('Тип процессора:' ): desc['cpu']['name'] = values[index+1]
            elif value.startswith('Количество ядер:'): desc['cpu']['core'] = int(values[index+1])
            # -- ram ------------------------------------------------------------------------------
#   size
            # -- storage --------------------------------------------------------------------------
#   size
            # -- flash-card -----------------------------------------------------------------------
#   type
            # -- sim-card -------------------------------------------------------------------------
#   count
#   type
            # -- os -------------------------------------------------------------------------------
            elif value.startswith('Операционная система:'): desc['os']['name'] = values[index+1]
            # -- display --------------------------------------------------------------------------
            elif value.startswith('Тип дисплея:'              ): desc['display']['type'] = values[index+1]
            elif value.startswith('Диагональ дисплея (дюйм):' ): desc['display']['size'] = float(values[index+1])
            elif value.startswith('Плотность пикселей (PPI): '): desc['display']['ppi' ] = int(value.split('Плотность пикселей (PPI): ' )[1])
            elif value.startswith('Разрешение дисплея (пикс):'):
                desc['display']['resolution-x'] = int(values[index+1].split('x')[0])
                desc['display']['resolution-y'] = int(values[index+1].split('x')[1])
            # -- physical -------------------------------------------------------------------------
            elif value.startswith('Высота (мм): '): desc['physical']['length']    = float(value.split('Высота (мм): ' )[1])
            elif value.startswith('Ширина (мм): '): desc['physical']['width']     = float(value.split('Ширина (мм): ' )[1])
            elif value.startswith('Толщина (мм):'): desc['physical']['thickness'] = float(value.split('Толщина (мм): ')[1])
            elif value.startswith('Вес (г): '    ): desc['physical']['weight']    = float(value.split('Вес (г): '     )[1])
            # color

        # media_content
        desc['media_content']['images'] = response.css('[rel="photo"] img::attr(src)').extract()
        # videos
        # sites

        # -- price --------------------------------------------------------------------------------
        price = defaultdict(dict)
        price['main']['BYN'] = float(response.css('#priceBox1::text').extract_first())
        # max
        # min
        # real

        yield {
            'url'       : response.url, 
            'timestamp' : timestamp.isoformat(),
            'idx'       : response.css('.b-offer-container__inner::attr(id)').extract_first().split('product-id-')[1],
            'title'     : response.css('.b-offer-title::text').extract_first().split(' ', 1)[1],
            'desc'		: desc,
            'price'     : price,
        }
