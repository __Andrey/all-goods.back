from django.core.management.base import BaseCommand
from apps.goods.tasks import load_data_to_db


class Command(BaseCommand):
    help = 'Load data to DB'

    def handle(self, *args, **options):
        load_data_to_db()
        # load_data_to_db.si().apply_async()
