from django.core.management.base import BaseCommand
from apps.goods.tasks import run_spider


class Command(BaseCommand):
    help = 'Run spider task.'

    def add_arguments(self, parser):
        parser.add_argument('spider', nargs='+', type=str)

    def handle(self, *args, **options):
        for spider in options['spider']:
            run_spider(spider=spider)
            # run_spider.si(spider=spider).apply_async()
