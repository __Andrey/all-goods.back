# -*- coding: utf-8 -*-
from django.db.models   import Model, CASCADE, ForeignKey, CharField, TextField, DateTimeField, BooleanField, IntegerField
from django.contrib.postgres.fields import JSONField


class Site(Model):
    url         = CharField     (max_length=256, unique=True)
    desc        = TextField     (default='')

    def __str__(self):
        return self.url 


class Page(Model):
    site        = ForeignKey    (Site, on_delete=CASCADE)
    url         = CharField     (max_length=512, unique=True,   help_text='Url without domain. You can find domain in site.url .')
    last_visit  = DateTimeField (                               help_text='When spider was on the page in last time.')

    def __str__(self):
        return "%s%s"%(self.site.url, self.url)


class Goods(Model):
    site        = ForeignKey    (Site, on_delete=CASCADE)
    idx         = CharField     (max_length=256,    help_text='ID or Slug.')
    last_updated= DateTimeField (                   help_text='Datetime from Result.timestamp')
    category    = CharField     (max_length=256,    help_text='Name of category.')
    link_id     = IntegerField  (default=None, blank=True, null=True, help_text='Id from category table')
    title       = CharField     (default=''  , blank=True, max_length=256, help_text='Title.')
    desc        = JSONField     (null=True,         help_text='Last fresh data that user apply from Result.desc .')
    price       = JSONField     (null=True,         help_text='Last fresh data that user apply from Result.price .')

    class Meta:
        unique_together = (("site", "idx"),)


class Session(Model):
    spider      = CharField     (max_length=128,    help_text='Name of spider format <category_folder>:<spider_file>')
    started     = DateTimeField ()
    finished    = DateTimeField (blank=True, null=True)
    checked     = BooleanField  (blank=True, default=False, help_text='User have checked this session.')

    class Meta:
        unique_together = (("spider", "started"),)

    def __str__(self):
        return "%s %s"%(self.started, self.spider)


class Result(Model):
    session     = ForeignKey    (Session, on_delete=CASCADE)
    page        = ForeignKey    (Page   , on_delete=CASCADE)
    goods       = ForeignKey    (Goods  , on_delete=CASCADE)
    timestamp   = DateTimeField (help_text='Datetime when data was read from page.')
    title       = CharField     (default='', blank=True, max_length=256, help_text='Title.')
    desc        = JSONField     (default=dict, help_text='Desc  that was scriped from page.')
    price       = JSONField     (default=dict, help_text='Price that was scriped from page.')

    class Meta:
        unique_together = (("session", "page", "goods"),)
