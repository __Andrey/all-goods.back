import os
import json
import dateutil.parser
from urllib.parse   import urlparse
from datetime       import datetime
from os.path        import join, splitext, isfile
from celery         import shared_task
from django.conf    import settings
from ..models import Site, Page, Goods, Session, Result


@shared_task(queue='parsers')
def load_data_to_db():

    for category in os.listdir(settings.DATA_DIR):
        for spider in os.listdir(os.path.join(settings.DATA_DIR, category)):
            feed_dir   = join(settings.DATA_DIR, category, spider)
            feed_files = [f for f in os.listdir(feed_dir) if isfile(join(feed_dir, f)) and splitext(f)[1] == '.json']
            for feed_file in feed_files: 

                session, created = Session.objects.get_or_create(
                    spider  = '%s:%s'%(category, spider), 
                    started = splitext(feed_file)[0])
                if not created:
                    continue
                session.save()

                site = None
                with open(os.path.join(feed_dir, feed_file)) as file:
                    for line in file:
                        print(line)
                        i = json.loads(line)
                        timestamp = dateutil.parser.parse(i['timestamp'])

                        url = urlparse(i['url'])
                        if not site:
                            site, created = Site.objects.get_or_create(url=url.netloc)

                        try:                        
                            page = Page.objects.get(site=site, url=url.path)
                            if page.last_visit < timestamp:
                                page.last_visit = timestamp 
                                page.save()
                        except Page.DoesNotExist:
                            page = Page(site=site, url=url.path, last_visit=timestamp)
                            page.save()

                        if i['idx'] is None:
                            # TODO: add warning
                            continue

                        try:
                            goods = Goods.objects.get(site=site, idx=i['idx'])
                            if goods.last_updated < timestamp:
                                goods.last_updated = timestamp 
                                goods.save()
                        except Goods.DoesNotExist:
                            goods = Goods(site          = site, 
                                          idx           = i['idx'], 
                                          category      = category, 
                                          last_updated  = timestamp, 
                                          title         = i.get('title', ''), 
                                          desc          = i.get('desc' , {}), 
                                          price         = i.get('price', {}))
                            goods.save()

                        result = Result(session     = session, 
                                        page        = page, 
                                        goods       = goods, 
                                        timestamp   = timestamp, 
                                        title       = i.get('title', ''),
                                        desc        = i.get('desc' , {}),
                                        price       = i.get('price', {}))
                        result.save()

                # TODO: archive original files after load
        