import logging
import os
from datetime       import datetime
from celery         import shared_task
from scrapy.crawler import CrawlerProcess
from django.conf    import settings
from django.utils.timezone import utc


@shared_task(queue='parsers')
def run_spider(spider):
    now            = datetime.utcnow().replace(tzinfo=utc)
    category, site = spider.split(':')
    tmp_dir        = os.path.join(settings.BASE_DIR, 'tmp', category, site)
    store_dir      = os.path.join(settings.DATA_DIR,        category, site)
    feed_file      = os.path.join(tmp_dir, "%s.json" % now)
    log_file       = os.path.join(tmp_dir, "%s.log"  % now)

    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)

    if not os.path.exists(store_dir):
        os.makedirs(store_dir)

    process = CrawlerProcess({
        'FEED_URI'      : 'file://'+feed_file,
        'FEED_FORMAT'   : 'jsonlines',

        'LOG_ENABLE'    : True,
        'LOG_FILE'      : log_file,
        'LOG_LEVEL'     : logging.INFO,
        'LOG_FORMAT'    : '%(levelname)s: %(message)s',

        # 'USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36'
    })
    spider_class  = getattr(__import__('apps.goods.category.%s.spiders.%s' % (category, site), fromlist=['Spider']), 'Spider')
    process.crawl(spider_class)
    process.stop()
    process.start()

    # move files
    # TODO: send file using by SFTP 
    os.rename(feed_file, os.path.join(store_dir, "%s.json" % now))
    os.rename(log_file , os.path.join(store_dir, "%s.log"  % now))
