# -*- coding: utf-8 -*-
import json
from rest_framework import filters


class JSDataFilterBackend(filters.BaseFilterBackend):
    """
    Filter for js-data queries  (http://www.js-data.io/docs/query-syntax)
    """
    def filter_queryset(self, request, queryset, view):
        query_params = dict(request.query_params)

        # TODO: It's filter isn't done yet! Complex cases isn't works (it's only AND)
        lookups = {
            '=='    : { 'postfix': '__exact'    , 'exclude': False},
            '==='   : { 'postfix': '__exact'    , 'exclude': False},
            'in'    : { 'postfix': '__in'       , 'exclude': False},
            'not_in': { 'postfix': '__in'       , 'exclude': True},
            '!='    : { 'postfix': '__exact'    , 'exclude': True},
            'like'  : { 'postfix': '__like'     , 'exclude': False},
            # TODO: add more lookups postfixes
        }

        where = query_params.get('where', {})
        if where:
            where = json.loads(where[0])
            for field in where.keys():
                for lookup, value in where[field].items():
                    # print ('exclude' if lookups[lookup]['exclude'] else '', queryset.model._meta.model_name,
                    #     {str(field.replace(".", "__")+lookups[lookup]['postfix']):value})
                    if lookups[lookup]['exclude']:
                        queryset = queryset.exclude(**{str(field.replace(".", "__")+lookups[lookup]['postfix']):value})
                    else:
                        queryset = queryset.filter (**{str(field.replace(".", "__")+lookups[lookup]['postfix']):value})

        # order by
        order_by = [('-' if json.loads(i)[1].upper()=='DESC' else '')+json.loads(i)[0] for i in query_params.get('order_by', [])]
        if order_by:
            queryset = queryset.order_by(*order_by)

        # limit and offset (can be use for pagination)
        limit = query_params.get('limit')
        offset = query_params.get('offset')
        if limit and offset:
            queryset = queryset[int(offset[0]):int(offset[0])+int(limit[0])]
        elif offset and not limit:
            queryset = queryset[int(offset[0]):]
        elif limit and not offset:
            queryset = queryset[:int(limit[0])]

        return queryset
