from .base import *

try:
    from .local import *
except ImportError:
    pass

if DEBUG:
    INSTALLED_APPS      += ['debug_toolbar', ]
    # Warning: You should include the Debug Toolbar middleware as early as possible in the list. 
    MIDDLEWARE           = ['debug_toolbar.middleware.DebugToolbarMiddleware', ] + MIDDLEWARE
    INTERNAL_IPS         = ('127.0.0.1',)
    DEBUG_TOOLBAR_CONFIG = { 'INTERCEPT_REDIRECTS': False }