"""all_goods URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from rest_framework import routers
from apps.goods.api.views import SiteViewSet, PageViewSet, GoodsViewSet, SessionViewSet, ResultViewSet
from apps.goods.category.smartphone.api.views import SmartphoneViewSet
from .views.get_settings import get_settings


router = routers.DefaultRouter(trailing_slash=True)

router.register(r'goods-site'   , SiteViewSet       )
router.register(r'goods-page'   , PageViewSet       )
router.register(r'goods-goods'  , GoodsViewSet      )
router.register(r'goods-session', SessionViewSet    )
router.register(r'goods-result' , ResultViewSet     )
router.register(r'goods-smartphone'      , SmartphoneViewSet     )

urlpatterns = [
    path('admin/'           , admin.site.urls),
	path('api/settings/'    , get_settings),
	path('api/'             , include(router.urls)),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

